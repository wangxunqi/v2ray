# Build
```
vim default.conf # Please change your server name
docker build -t v2ray .
```

# Run
```
docker run --name v2ray --restart=always -v /path/to/certificate:/etc/nginx/ssl:ro -d -p 80:80 -p 443:443 v2ray
docker exec -it v2ray /usr/bin/v2ray/v2ray -config /etc/v2ray/config.json
```
