# A V2ray container designed for PaaS
[![Build Status](https://travis-ci.org/wangxunqi/v2ray.svg?branch=master)](https://travis-ci.org/wangxunqi/v2ray)
```
docker pull wangxunqi/v2ray
docker run --name v2ray --restart=always -d -p 80:8080 wangxunqi/v2ray
```
