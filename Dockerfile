FROM nginx:mainline

COPY config.json /etc/v2ray/

RUN apt update; \
    apt install curl wget unzip procps daemon -y; \
    curl -sL https://install.direct/go.sh | bash; \
    rm -rf /var/lib/apt/lists/*

USER nobody

EXPOSE 8080

CMD ["/usr/bin/v2ray/v2ray", "-config", "/etc/v2ray/config.json"]
